import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as io from 'socket.io-client';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ChatsService {
  private url = "http://localhost:5050";
  public socket;
  public udata: string = '';
  
  constructor(private http:HttpClient,private route:Router) { 
    this.socket = io(this.url);

    this.socket.on("test", (param) => {
      console.log("this is called", param);
    });

    this.socket.on("data",()=>{
      console.log("socket ping is on");
      this.login(sessionStorage.getItem("loginId"));
    });
  }

  public recData = new BehaviorSubject({});
  mRecData = this.recData.asObservable();

  changeData(name: string, imgUrl : string, status : string) {
    this.recData.next({name : name, image :imgUrl, status: status});
  }

  login(id): void {
    console.log("login is called");
    this.socket.emit('login', {id : id});
    this.socket.emit("checkOfflineOnline",sessionStorage.getItem("loginId"),localStorage.getItem("roomId"));
  }

  startType(recId){
    this.socket.emit("typing",recId);
  }

  stopType(recId){
    this.socket.emit("stoptype",recId);
  }

  getLiveStatus(){ console.log("tst");
    this.socket.emit("checkLiveStatus",sessionStorage.getItem("loginId"));
  }

  saveChats(messages){
    const headers = new HttpHeaders();
    headers.append("Content-type","application/json");
    headers.append("Accept","application/json");
    let sendMessage = {roomId:localStorage.getItem("roomId"),senderId:sessionStorage.getItem("loginId"),messages:messages,contentType:"application/json",createdAt:Date.now()};
    this.socket.emit("new message",messages);
    return this.http.post("http://localhost:5050/messages/saveChats",sendMessage,{headers:headers});
  }

  public checkTyping = ()=>{
    return Observable.create((observer)=>{
      this.socket.on("isTyping",(data)=>{
        observer.next(data);
      });
    });
  }

  public checkstatus = ()=>{
    return Observable.create((observer)=>{
      this.socket.on("OnlineOffline",(data)=>{
        observer.next(data);
      });
    });
  }

  public checkLiveStatus = ()=>{
    return Observable.create((observer)=>{
      this.socket.on("userLiveStatus",(data)=>{
          observer.next(data);
      })
    });
  }

  public getMessages = () => {
      return Observable.create((observer) => {
          this.socket.on('onMessageRecieved', (message) => {
              console.log("<<<<<<<<<>>>>>>>>>>",message);
              observer.next(message);
          });
      });
  }

  logout(){
    this.socket.emit("logout",sessionStorage.getItem("loginId"),localStorage.getItem("roomId"));
    sessionStorage.clear();
    localStorage.clear();
    this.route.navigateByUrl("");
  }
}
