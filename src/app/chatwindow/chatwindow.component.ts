import { Component, OnInit,AfterViewChecked, ElementRef, ViewChild, Inject } from '@angular/core';
import { ChatsService } from '../chats.service';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { fadeInOut } from '../flyanimation';

@Component({
  selector: 'app-chatwindow',
  templateUrl: './chatwindow.component.html',
  styleUrls: ['./chatwindow.component.css'],
  animations: [fadeInOut]
})

export class ChatwindowComponent implements OnInit, AfterViewChecked {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  animal : string;
  name : string;
  chatMessage : string;
  loginname : string;
  chatData : any;
  messages : string;
  loginId : string;
  userChats : any[] = [];
  userData : any;
  fullname:string;
  image:string;
  msgcount:number=0;
  recieverImg:string='';
  senderImg:string='';
  userdata:any;
  offlineOnline:string='offline';
  firstname: string= sessionStorage.getItem("firstname");
  lastname: string= sessionStorage.getItem("lastname");
  about: string=sessionStorage.getItem("about");
  phone: string=sessionStorage.getItem("phone");
  type:string = "";

  constructor(private chats:ChatsService,private http:HttpClient,private route:Router) {
      this.chats.login(sessionStorage.getItem("loginId"));
      this.chats.changeData(sessionStorage.getItem("recName"),sessionStorage.getItem("recImage"),sessionStorage.getItem("recStatus"));
  }

  ngAfterViewChecked(){
    this.scrollToBottom();
  }
  ngOnInit() {
    this.loginname = sessionStorage.getItem("fullname");
    this.chats.mRecData.subscribe(userdata=>this.userdata = userdata);
    $(document).ready(function(){
      $('#action_menu_btn').click(function(){
        $('.action_menu').toggle();
      });
      $(".send_btn").click(function(){
        $("textarea").val("");
      });
      $("textarea").keyup(function(e){
        if(e.which == 13 && $("textarea").val() != ""){
            $(".send_btn").trigger("click");
            document.getElementById('messages').scrollIntoView();
        }
      });
    });
    this.scrollToBottom();
    
    this.http.get("http://localhost:5050/messages/getChats/"+localStorage.getItem("roomId")).subscribe(data=>{ 
      this.chatData = JSON.parse(JSON.stringify(data));
      console.log(this.chatData);
      this.chatData.data.forEach(key=>{
        this.userChats.push(key);
      });
      this.msgcount = this.userChats ? this.userChats.length : 0;
      this.loginId = sessionStorage.getItem("loginId");
      this.senderImg = sessionStorage.getItem("senderImage");
    });

    this.chats.checkstatus().subscribe((userdata: string)=>{
       console.log("userdata",userdata);
        this.offlineOnline  = userdata;
    });

    this.chats.checkTyping().subscribe((typing:string)=>{
        console.log("istyping",typing);
        this.type = '';
        this.type = typing;
    });

    this.chats
        .getMessages()
        .subscribe((message: string) => {
          this.userChats.push(message);
          this.msgcount = this.userChats ? this.userChats.length : 0;
          this.loginId = sessionStorage.getItem("loginId");
          this.senderImg = sessionStorage.getItem("senderImage");
        });
    }

  scrollToBottom(): void {
      try {
          this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
      } catch(err) { }                 
  }

  startTyping(e){
   if(e.keyCode != 9 && e.keyCode != 18)
      this.chats.startType(sessionStorage.getItem("recId"));
  }

  stopTyping(e){
    if(e.keyCode != 9 && e.keyCode != 18)
      this.chats.stopType(sessionStorage.getItem("recId"));
  }

  sendMessage(){
    this.chats.saveChats(this.chatMessage).subscribe(data=>{
      this.chatData = JSON.parse(JSON.stringify(data));
      if(!this.chatData.error){
        this.http.get("http://localhost:5050/messages/getMessage/"+this.chatData.data['_id']).subscribe(data=>{ 
          this.chatData = JSON.parse(JSON.stringify(data));
          this.chatData.data.forEach(key=>{
            this.userChats.push(key);
          });
          this.msgcount = this.userChats ? this.userChats.length : 0;
          this.loginId = sessionStorage.getItem("loginId");
          this.senderImg = sessionStorage.getItem("senderImage");
        });
      }
    });
  }

  logout(){
    this.chats.logout();
  }
}