import { trigger, state, style, animate, transition } from '@angular/animations';

export const flyanimation = trigger('EnterLeave', [
    state('flyIn', style({ transform: 'translateX(0)' })),
    transition(':enter', [
      style({ transform: 'translateX(-100%)' }),
      animate('0.5s 900ms ease-in')
    ]),
    transition(':leave', [
      animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
    ])
]);

export const fadeInOut = trigger('FadeInOut',[
    state('void',style({
        opacity:0
    })),
    transition('void <=> *',animate(1000)),
]);
