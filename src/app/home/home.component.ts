import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../rooms.service';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';
import { ChatsService } from '../chats.service';
import { fadeInOut } from '../flyanimation';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [fadeInOut]
})
export class HomeComponent implements OnInit {
  roomlist : any[] = [];
  userData : string;
  fullname : string;
  userLiveData : string;

  constructor(private rooms:RoomsService,private route:Router,private user:UsersService,private chats:ChatsService) { 
    this.chats.login(sessionStorage.getItem("loginId"));
    this.chats.getLiveStatus();
    
    this.chats.checkLiveStatus().subscribe(data=>{
      this.userLiveData = JSON.parse(JSON.stringify(data));
      console.log("userLivedata: ",this.userLiveData);
    });

  }
  
  ngOnInit() {
    
    this.user.getUserDetail().subscribe(data=>{
      this.userData = JSON.parse(JSON.stringify(data));
      this.fullname = this.userData['data'].firstname + " " + this.userData['data'].lastname;
      sessionStorage.setItem("fullname",this.fullname);
    });

    this.rooms.getRooms().subscribe(data=>{
      this.rooms = JSON.parse(JSON.stringify(data));
      this.rooms['data'].forEach(key=>{
          key.roomlist.forEach(user=>{
            user['room_id'] = key['_id'];
            if(user['_id'] != sessionStorage.getItem("loginId")){
                this.roomlist.push(user);
            }
          });
      });
    });
  }

  redirectToChat(roomId,imageUrl,firstname,lastname, status,recId){
    let name = firstname + " " + lastname;
    this.chats.changeData(name,imageUrl,status);
    localStorage.setItem("roomId",roomId);
    sessionStorage.setItem("recImage",imageUrl);
    sessionStorage.setItem("recName",name);
    sessionStorage.setItem("recId",recId);
    sessionStorage.setItem("recStatus",status);
    this.route.navigateByUrl("chatWindow");
  }
}
