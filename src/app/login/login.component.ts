import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Userlogin } from '../userlogin';
import { UsersService } from '../users.service';
import { ChatsService } from '../chats.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  data : any;
  error : Boolean = false;
  constructor(private http:HttpClient,private route : Router,private users:UsersService,
    private chats:ChatsService) { }

  ngOnInit() {
    if(sessionStorage.getItem("isLogin") == "true"){
      this.route.navigateByUrl("home");
    }
  }

  checkLogin(userdata : Userlogin){
    this.users.login(userdata.username,userdata.password).subscribe(data=>{
      this.data = JSON.parse(JSON.stringify(data));
      if(!this.data.error){
        sessionStorage.setItem("loginId",this.data.data._id);
        sessionStorage.setItem("senderImage",this.data.data.imageUrl);
        sessionStorage.setItem("firstname",this.data.data.firstname);
        sessionStorage.setItem("lastname",this.data.data.lastname);
        sessionStorage.setItem("about",this.data.data.about);
        sessionStorage.setItem("phone",this.data.data.phone);
        this.chats.login(this.data.data._id);
        this.route.navigateByUrl("home");
      } else {
        this.error = true;
      }
    },error=>{
      return error;
    });
  }
}
