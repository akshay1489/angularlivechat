import { Component, OnInit } from '@angular/core';
import { Userregister } from '../userregister';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsersService } from '../users.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  selectedFile : any;
  data : any;
  error : boolean = false;
  constructor(private http:HttpClient,private users:UsersService,private route:Router) { }

  ngOnInit() {
  }

  uploadImage(event){
    this.selectedFile = <File> event.target.files[0];
  }
  
  registerUser(values : Userregister){ 
    const headers = new HttpHeaders();
    headers.append("Content-type","multipart/form-data");
    headers.append("Accept","application/json");
    let selectedFile = this.selectedFile;
    let uploadedData = new FormData();
    uploadedData.append("profilepic",selectedFile,selectedFile.name);
    uploadedData.append("firstname",values.firstname);
    uploadedData.append("lastname",values.lastname);
    uploadedData.append("phone",values.phone);
    uploadedData.append("status","offline");
    uploadedData.append("about",values.about);
    uploadedData.append("username",values.username);
    uploadedData.append("password",values.password);
    this.users.register(uploadedData,headers).subscribe(data=>{
        this.data = JSON.parse(JSON.stringify(data));
        this.error = this.data.error;
        if(!this.data.error){          
          console.log("User registered successfully!!!");
          this.route.navigateByUrl('/login');
        } else {
          console.log("User registration failed!!!");
        }
    });
  }
}