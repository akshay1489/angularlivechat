import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  constructor(private http:HttpClient) { }

  saveRooms(users){
    const headers = new HttpHeaders();
    headers.append("Content-type","application/json");
    headers.append("Accept","application/json");
    let sendData = {isGroup:false,users:users,groupPicUrl:"",groupStatus:"",groupName:""};
    return this.http.post("http://localhost:5050/rooms/saveRooms",sendData,{headers:headers});
  }

  getRooms(){
    return this.http.get("http://localhost:5050/rooms/listrooms/"+sessionStorage.getItem('loginId'));
  }
}
