import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { RoomsService } from '../rooms.service';
import { Router } from '@angular/router'; 
import { ChatsService } from '../chats.service';
import { fadeInOut } from '../flyanimation';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  animations: [fadeInOut]
})
export class SearchComponent implements OnInit {
  userList : any[] = [];
  searchKey : string;
  chatUsers : any[] = [];
  data : any;
  fullname: string;
  
  constructor(private users:UsersService,private rooms:RoomsService,private route:Router,private chats:ChatsService) {
      this.chatUsers.push(sessionStorage.getItem('loginId'));
   }

  ngOnInit() {
    this.fullname = sessionStorage.getItem("fullname");
  }

  getAllUsers(){
    this.userList = [];
    if(this.searchKey){
      this.users.getUsers(this.searchKey).subscribe(data=>{
        let udata = JSON.parse(JSON.stringify(data));
        udata.data.forEach(key=>{
          if(key._id != sessionStorage.getItem('loginId')){
            this.userList.push(key);
          }
        });
      });
    } else {
      this.userList = [];
    }
  }

  createRoom(userId,imageUrl,firstname,lastname,status){
    let name = firstname + " " + lastname;
    this.chatUsers.push(userId);
    this.rooms.saveRooms(this.chatUsers.join(",")).subscribe(data=>{
      this.data = JSON.parse(JSON.stringify(data));
      if(this.data.data.users.length > 0){
        this.chats.changeData(name,imageUrl,status);
        sessionStorage.setItem("recImage",imageUrl);
        sessionStorage.setItem("recName",name); 
        sessionStorage.setItem("recStatus",status); 
        localStorage.setItem("roomId",this.data.data._id);
        this.route.navigateByUrl('chatWindow');
      }
    });
  }

}
