export class Userregister {
    firstname : string;
    lastname : string;
    phone : string;
    imageUrl : string;
    about : string;
    status : {type:string,default:"offline"};
    username : string;
    password : string;
}
