import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Userlogin } from './userlogin';
import { Userregister } from './userregister';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http:HttpClient) { }

  login(username,password) :Observable<Userlogin>{
    sessionStorage.setItem("isLogin","true");
    return this.http.get<Userlogin>("http://localhost:5050/login/"+username+"/"+password);
  }

  register(uploadedData,headers) : Observable<Userregister>{
    return this.http.post<Userregister>("http://localhost:5050/register",uploadedData,{headers:headers});
  }

  getUsers(event) : Observable<Userregister>{
      return this.http.get<Userregister>('http://localhost:5050/getUsers/'+event);
  }

  getUserDetail() : Observable<Userregister>{
      return this.http.get<Userregister>("http://localhost:5050/getUserDetail/"+sessionStorage.getItem("loginId"));
  }
}
